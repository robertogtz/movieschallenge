//
//  RappiChallengeUITests.swift
//  RappiChallengeUITests
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import XCTest

class RappiChallengeUITests: XCTestCase {

    override func setUp() {
    
        continueAfterFailure = false

        XCUIApplication().launch()

    }

    override func tearDown() {
        
    }
    
    func testUIHomeScreenElementsExist() {
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let homeCell = tablesQuery.children(matching: .cell).matching(identifier: "HomeCellIdentifier").element(boundBy: 0)
        let homeCellTitleLabel = homeCell.staticTexts["HomeTitleLabelIdentifier"]
        
        XCTAssert(homeCell.exists, "Home cells should exists")
        XCTAssert(homeCellTitleLabel.exists, "Home cell title label should exists")
    }
    
    func testUIMovieListViewElementsExist() {
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        let homeCell = tablesQuery.children(matching: .cell).matching(identifier: "HomeCellIdentifier").element(boundBy: 0)
        let homeCellTitleLabel = homeCell.staticTexts["HomeTitleLabelIdentifier"]
        homeCellTitleLabel.tap()
        
        let movieListCell = tablesQuery.children(matching: .cell).matching(identifier: "MovieListCellIdentifier").element(boundBy: 0)
        let movieCellTitleLabel = movieListCell.staticTexts["MovieTitleIdentifier"]
        
        XCTAssert(movieListCell.exists, "Movie list cells should exists")
        XCTAssert(movieCellTitleLabel.exists, "Movie list cell title label should exists")
    }

    func testUIDetailViewElementsExist() {
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.children(matching: .cell).matching(identifier: "HomeCellIdentifier").element(boundBy: 0).staticTexts["HomeTitleLabelIdentifier"].tap()
        tablesQuery.children(matching: .cell).matching(identifier: "MovieListCellIdentifier").element(boundBy: 0).staticTexts["MovieTitleIdentifier"].tap()
        
        XCTAssert(app.textViews["DetailOverviewIdentifier"].exists, "DetailView overview label should exist")
        XCTAssert(app.staticTexts["DetailTitleIdentifier"].exists, "DetailView title label should exist")
        XCTAssert(app.staticTexts["DetailReleaseIdentifier"].exists, "DetailView release label should exist")
    }

}
