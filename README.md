# MOVIES CHALLENGE iOS #
*The iOS code base for Rappi code challenge*

## iOS Deployment Target
- iOS 12.2

## Application description ##
- Clean Architecture i create 3 Scenes: HomeScene is in charge of showing movie category list, MovieListScene show list on movies on each category, MovieDetailScene show detail of movie.
- URLConnections were made from scratch with URLSession (NetworkManager class handles all connections).
- For persistance i used a class called Storage wich is based in Codable protocol to store data on device.
- TableViewDataSource is a generic tableView Handler, is in charge of configuring cell and detect selection, this Class accepts 2 generic parameters, CellType and Model.
- Unit Test and UITest were added.

## Tools ##
- Xcode (10.2.1)
- I didnt use any pods, i wanted to create all from scratch but i know how to use and create pods, i can accomplish the same with pods if needed. Cocoapods - [The Cocoa Dependency Manager](https://cocoapods.org/)

## Language ##
- Swift (4.2)

## Questions

- What is SOLID Single responsability means? means that every module, class, or function should have responsibility over a single part of the functionality provided by the software, and that responsibility should be entirely encapsulated by the class.

- What a clean code should have?  For me a clean code should be testable, reusable, easy to understand and to refactor.
 
