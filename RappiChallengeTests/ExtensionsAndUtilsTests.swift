//
//  RappiChallengeTests.swift
//  RappiChallengeTests
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import XCTest
@testable import RappiChallenge

class ExtensionsAndUtilsTests: XCTestCase, CategoriesMockable {
    
    private struct Constants {
        static let categoriesFileName = "mockedCategories.json"
    }

    var mockedCategories: AllCategoriesModel?
    
    override func setUp() {
        mockedCategories = self.provideMockedCategories()
        Storage.store(mockedCategories, to: .caches, as: Constants.categoriesFileName)
    }

    override func tearDown() {
        Storage.clear(.caches)
        mockedCategories = nil
    }
    
    // MARK:- Storage Tests

    func testStorageRetrieve() {
        Storage.store(mockedCategories, to: .caches, as: Constants.categoriesFileName)
        let retrievedCategories = Storage.retrieve(Constants.categoriesFileName, from: .caches, as: AllCategoriesModel.self)
        XCTAssert(retrievedCategories.count > 0, "App didnt save categories correctly")
    }
    
    func testStorageFileExist() {
        Storage.store(mockedCategories, to: .caches, as: Constants.categoriesFileName)
        XCTAssert(Storage.fileExists(Constants.categoriesFileName, in: .caches), "File should exist as expected")
    }
    
    func testStorageRemove() {
        Storage.store(mockedCategories, to: .caches, as: Constants.categoriesFileName)
        Storage.remove(Constants.categoriesFileName, from: .caches)
        XCTAssertFalse(Storage.fileExists(Constants.categoriesFileName, in: .caches), "File should be removed as expected")
    }
    
    func testStorageClear() {
        Storage.store(mockedCategories, to: .caches, as: Constants.categoriesFileName)
        Storage.clear(.caches)
        XCTAssertFalse(Storage.fileExists(Constants.categoriesFileName, in: .caches), "File should be removed as expected")
    }
    
    // MARK:- UIColor extension test
    
    func testRandomColor() {
        let color = UIColor().getRandomColor()
        XCTAssertNotNil(color, "getRandomColor returns a UIColor and should never be nil")
    }

    // MARK:- Date extension test
    

    func testDateExtension() {
        let popularCategory = self.provideMockedCategories().first
        let movies = popularCategory?.first?.value
        
        let releaseDate1 = movies?.first?.releaseDate.dateFromString().asString(style: .long)
        let releaseDate2 = movies?[1].releaseDate.dateFromString().asString(style: .long)
        let releaseDate3 = movies?[2].releaseDate.dateFromString().asString(style: .long)
        
        XCTAssert(releaseDate1 == "April 20, 2019", "Date formatter is not working as expected")
        XCTAssert(releaseDate2 == "January 28, 2019", "Date formatter is not working as expected")
        XCTAssert(releaseDate3 == "February 16, 2019", "Date formatter is not working as expected")
    }
}
