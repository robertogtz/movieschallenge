//
//  CategoriesMockable.swift
//  RappiChallengeTests
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import Foundation
@testable import RappiChallenge

protocol CategoriesMockable { }

extension CategoriesMockable {
    
    func provideMockedCategories() -> AllCategoriesModel {
        var categories = AllCategoriesModel()
        
        var category1 = CategoryModel()
        category1["popular"] = [
            Movie(voteCount: 5, id: 1, video: false, voteAverage: 4.0, title: "Test Movie 1", popularity: 9.0, posterPath: "/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg", originalLanguage: "en", originalTitle: "Test Movie 2", genreIDS: [1,3,4], backdropPath: "/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg", adult: false, overview: "overview test 1", releaseDate: "2019-04-20"),
            Movie(voteCount: 5, id: 2, video: false, voteAverage: 4.0, title: "Test Movie 2", popularity: 9.0, posterPath: "/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg", originalLanguage: "en", originalTitle: "Test Movie 2", genreIDS: [1,3,4], backdropPath: "/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg", adult: false, overview: "overview test 2", releaseDate: "2019-01-28"),
            Movie(voteCount: 5, id: 3, video: false, voteAverage: 4.0, title: "Test Movie 3", popularity: 9.0, posterPath: "/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg", originalLanguage: "en", originalTitle: "Test Movie 2", genreIDS: [1,3,4], backdropPath: "/w9kR8qbmQ01HwnvK4alvnQ2ca0L.jpg", adult: false, overview: "overview test 3", releaseDate: "2019-02-16")
        ]
        
        categories.append(category1)
        
        return categories
    }
}
