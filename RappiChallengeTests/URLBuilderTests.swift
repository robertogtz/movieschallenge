//
//  URLBuilderTests.swift
//  RappiChallengeTests
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import XCTest
@testable import RappiChallenge

class URLBuilderTests: XCTestCase {

    override func setUp() {
        
    }

    func testURLBuilderPopularMoviesURL() {
        let popularMoviesURL = URLBuilder.url(for: .popular).absoluteString
        XCTAssert(popularMoviesURL == "https://api.themoviedb.org/3/movie/popular?api_key=d7f77038021a3685a9b4fcea36ecdb4b", "URL Builder didnt construct popular movies url as expected")
    }
    
    func testURLBuilderUpcomingMoviesURL() {
        let upcomingMoviesURL = URLBuilder.url(for: .upcoming).absoluteString
        XCTAssert(upcomingMoviesURL == "https://api.themoviedb.org/3/movie/upcoming?api_key=d7f77038021a3685a9b4fcea36ecdb4b", "URL Builder didnt construct upcoming movies url as expected")
    }
    
    func testURLBuilderTopRatedMoviesURL() {
        let topRatedMoviesURL = URLBuilder.url(for: .topRated).absoluteString
        XCTAssert(topRatedMoviesURL == "https://api.themoviedb.org/3/movie/top_rated?api_key=d7f77038021a3685a9b4fcea36ecdb4b", "URL Builder didnt construct upcoming movies url as expected")
    }

}
