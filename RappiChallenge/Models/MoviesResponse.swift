//
//  MoviesResponse.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import Foundation

struct MoviesResponse: Codable {
    let results: [Movie]?
}


