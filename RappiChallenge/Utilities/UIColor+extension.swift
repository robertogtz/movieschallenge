//
//  UIColor+extension.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

extension UIColor {
    
    public convenience init(r: Int, g: Int, b: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
    
    func getRandomColor() -> UIColor {
        let colorsArray: [UIColor] = [.customOrange, .customBlue, .customPurple, .customYellow, .customGreen, .customBlueGreen]
        let randomIndex = Int(arc4random_uniform(UInt32(colorsArray.count)))
        return colorsArray[randomIndex]
    }
    
    static let customOrange: UIColor    = UIColor(r: 234, g: 32, b: 39)
    static let customBlue: UIColor      = UIColor(r: 0, g: 168, b: 255)
    static let customPurple: UIColor    = UIColor(r: 156, g: 136, b: 255)
    static let customYellow: UIColor    = UIColor(r: 251, g: 197, b: 49)
    static let customGreen: UIColor     = UIColor(r: 196, g: 229, b: 56)
    static let customBlueGreen: UIColor = UIColor(r: 18, g: 203, b: 196)
}
