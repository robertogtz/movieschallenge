//
//  UIImage+extension.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    
    //MARK: - Constants
    private struct Constants {
        static let imageBaseURL   = "https://image.tmdb.org/t/p/w500"
    }
    
    func cacheImage(urlString: String) {
        let fullURL = Constants.imageBaseURL + urlString
        guard let url = URL(string: fullURL) else { return }
        
        image = nil
        
        if let imageFromCache = imageCache.object(forKey: fullURL as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        URLSession.shared.dataTask(with: url) {
            data, response, error in
            if let data = data {
                DispatchQueue.main.async {
                    guard let imageToCache = UIImage(data: data) else { return }
                    imageCache.setObject(imageToCache, forKey: fullURL as AnyObject)
                    self.image = imageToCache
                }
            }
        }.resume()
    }
}
