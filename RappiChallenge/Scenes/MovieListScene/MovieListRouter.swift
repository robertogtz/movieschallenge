//
//  MovieListRouter.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

@objc protocol MovieListRoutingLogic {
    func routeToMovieDetail()
}

protocol MovieListDataPassing {
    var dataStore: MovieListDataStore? { get }
}

class MovieListRouter: NSObject, MovieListRoutingLogic, MovieListDataPassing {
    
    weak var viewController: MovieListViewController?
    var dataStore: MovieListDataStore?
    
    func routeToMovieDetail() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToComments(source: dataStore!, destination: &destinationDS)
        navigateToComments(source: viewController!, destination: destinationVC)
    }
    
    // MARK: Navigation
    func navigateToComments(source: MovieListViewController, destination: MovieDetailViewController) {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    func passDataToComments(source: MovieListDataStore, destination: inout MovieDetailDataStore) {
        destination.movie = source.movie
    }
}
