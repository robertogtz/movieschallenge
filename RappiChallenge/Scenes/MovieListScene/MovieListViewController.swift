//
//  MovieListViewController.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol MovieListDisplayLogic: class {
    func displayMovieDetailPage(viewModel: MovieList.Movies.ViewModel)
}

class MovieListViewController: UIViewController {
    
    // MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    private var tableViewHandler: TableViewDataSource<MovieTableViewCell, Movie>?
    
    // MARK:- Constants
    private struct Constants {
        static let cellIdentifier = "MovieListCell"
        static let searchMessage = "Search your favorite movie!"
    }
    
    // MARK:- Variables
    private var movies = [Movie]()
    private var filteredMovies = [Movie]()
    private let searchController = UISearchController(searchResultsController: nil)
  
    var interactor: MovieListBusinessLogic?
    var router: (NSObjectProtocol & MovieListRoutingLogic & MovieListDataPassing)?

    // MARK: Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupCleanArchStack()
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCleanArchStack()
    }
  
    // MARK: Setup
  
    private func setupCleanArchStack() {
        let viewController = self
        let interactor = MovieListInteractor()
        let presenter = MovieListPresenter()
        let router = MovieListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        self.setupUI()
    }
    
    private func setupUI() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .never
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.tintColor = .white
        searchController.searchBar.placeholder = Constants.searchMessage
        searchController.searchBar.barTintColor = .white
        searchController.searchResultsUpdater = self
        self.navigationItem.searchController = searchController
    }
    
    private func setupTableView(_ movies: [Movie]) {
        self.tableView.separatorStyle = .none
        self.tableViewHandler = TableViewDataSource<MovieTableViewCell, Movie>(Constants.cellIdentifier, movies, cellConfigurationBlock: { (cell, movie) in
            
            let releaseDate = movie.releaseDate.dateFromString().asString(style: .long)
            cell.titleLabel?.text = movie.title
            cell.releaseDateLabel.text =  "Release Date: \(releaseDate)"
            cell.posterImage.cacheImage(urlString: movie.backdropPath)
            
        }, cellSelectorHandler: { [weak self] (movie) in
        
            self?.dismissSearchBar()
            let request = MovieList.Movies.Request(movie: movie)
            self?.interactor?.doLoadMovie(request: request)
            
        })
        self.tableView.dataSource = tableViewHandler
        self.tableView.delegate = tableViewHandler
    }
  
    // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        loadInitialData()
    }
  
    func loadInitialData() {
        self.movies = router?.dataStore?.category.values.first ?? [Movie]()
        self.title = router?.dataStore?.category.keys.first ?? ""
        self.setupTableView(self.movies)
        self.tableView.reloadData()
    }

}

extension MovieListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let input = searchController.searchBar.text else { return }
        filterContentForSearchText(input)
    }
    
    // MARK: - UISearchBar helper methods
    
    private func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    private func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredMovies = movies.filter({( movie : Movie) -> Bool in
            return (movie.title.lowercased().contains(searchText.lowercased()))
        })
        self.setupTableView(isFiltering() ? filteredMovies : movies)
        tableView.reloadData()
    }
    
    private func dismissSearchBar() {
        self.searchController.searchBar.text = ""
        self.searchController.dismiss(animated: true, completion: nil)
    }
}

extension MovieListViewController: MovieListDisplayLogic {
    
    func displayMovieDetailPage(viewModel: MovieList.Movies.ViewModel) {
        router?.routeToMovieDetail()
    }
}
