//
//  RondedImageView.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

class RondedImageView: UIImageView {

    override func awakeFromNib() {
        setupView()
    }
    
    func setupView() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
        self.layer.allowsEdgeAntialiasing = true
        self.contentMode = .scaleAspectFill
        self.layer.borderWidth = 1.5
        self.layer.borderColor = UIColor.white.cgColor
    }
}
