//
//  MovieTableViewCell.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var movieView: MovieView!
    @IBOutlet weak var posterImage: RondedImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        movieView.backgroundColor = UIColor().getRandomColor()
    }
}
