//
//  MovieListModels.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

enum MovieList {
  
    // MARK: Use cases
    enum Movies {
        struct Request {
            let movie: Movie
        }
    
        struct Response {
            let movie: Movie
        }
    
        struct ViewModel {
            let movie: Movie
        }
    }
}
