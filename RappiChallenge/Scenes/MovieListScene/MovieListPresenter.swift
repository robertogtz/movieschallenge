//
//  MovieListPresenter.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol MovieListPresentationLogic {
    func presentMovieDetail(response: MovieList.Movies.Response)
}

class MovieListPresenter: MovieListPresentationLogic {
  
    weak var viewController: MovieListDisplayLogic?
  
    func presentMovieDetail(response: MovieList.Movies.Response) {
        let viewModel = MovieList.Movies.ViewModel(movie: response.movie)
        viewController?.displayMovieDetailPage(viewModel: viewModel)
    }
}
