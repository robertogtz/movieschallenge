//
//  MovieListInteractor.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol MovieListBusinessLogic {
    func doLoadMovie(request: MovieList.Movies.Request)
}

protocol MovieListDataStore {
    var category: CategoryModel { get set }
    var movie: Movie? { get set }
}

class MovieListInteractor: MovieListBusinessLogic, MovieListDataStore {
    
    var category = CategoryModel()
    var movie: Movie?

    var presenter: MovieListPresentationLogic?
    var worker: MovieListWorker?
    
    func doLoadMovie(request: MovieList.Movies.Request) {
        self.movie = request.movie
        let response = MovieList.Movies.Response(movie: self.movie!)
        presenter?.presentMovieDetail(response: response)
    }
}
