//
//  MovieDetailViewController.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol MovieDetailDisplayLogic: class {
    
}

class MovieDetailViewController: UIViewController, MovieDetailDisplayLogic {
  
    // MARK: IBOutlets
    @IBOutlet weak var moviePosterView: ShadowedImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    
    
    // MARK: Variables
    private var movie: Movie?
    var interactor: MovieDetailBusinessLogic?
    var router: (NSObjectProtocol & MovieDetailRoutingLogic & MovieDetailDataPassing)?

    // MARK: Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupCleanArchStack()
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCleanArchStack()
    }
  
    // MARK: Setup
  
    private func setupCleanArchStack() {
        let viewController = self
        let interactor = MovieDetailInteractor()
        let presenter = MovieDetailPresenter()
        let router = MovieDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    private func setupUI() {
        guard let movie = self.movie else { return }
        self.title = movie.title
        self.moviePosterView.cacheImage(urlString: movie.backdropPath)
        self.moviePosterView.clipsToBounds = true
        let releaseDate = movie.releaseDate.dateFromString().asString(style: .long)
        self.movieTitleLabel.text = movie.title
        self.releaseDateLabel.text =  "Release Date: \(releaseDate)"
        self.overviewTextView.text = movie.overview
    }
  
    // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.movie = router?.dataStore?.movie
        self.setupUI()
    }
    
}
