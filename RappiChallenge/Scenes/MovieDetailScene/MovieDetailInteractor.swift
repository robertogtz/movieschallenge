//
//  MovieDetailInteractor.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol MovieDetailBusinessLogic {

}

protocol MovieDetailDataStore {
    var movie: Movie? { get set }
}

class MovieDetailInteractor: MovieDetailBusinessLogic, MovieDetailDataStore {

    var movie: Movie?
    var presenter: MovieDetailPresentationLogic?
    var worker: MovieDetailWorker?

}
