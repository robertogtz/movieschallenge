//
//  MovieDetailPresenter.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol MovieDetailPresentationLogic {

}

class MovieDetailPresenter: MovieDetailPresentationLogic {
  
    weak var viewController: MovieDetailDisplayLogic?
}
