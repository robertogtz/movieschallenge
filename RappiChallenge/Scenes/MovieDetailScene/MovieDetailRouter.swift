//
//  MovieDetailRouter.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

@objc protocol MovieDetailRoutingLogic {
    
}

protocol MovieDetailDataPassing {
    var dataStore: MovieDetailDataStore? { get }
}

class MovieDetailRouter: NSObject, MovieDetailRoutingLogic, MovieDetailDataPassing {
  
    weak var viewController: MovieDetailViewController?
    var dataStore: MovieDetailDataStore?
}
