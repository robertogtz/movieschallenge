//
//  MovieDetailModels.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

enum MovieDetail {
  
    enum Detail {
        struct Request {}
        struct Response {}
        struct ViewModel {}
    }
}
