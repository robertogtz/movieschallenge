//
//  HomeTableViewCell.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/7/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var homeCellView: HomeCellView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        homeCellView.backgroundColor = UIColor().getRandomColor()
    }
}
