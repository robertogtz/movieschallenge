//
//  HomeViewController.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol HomeDisplayLogic: class {
    func displayInitialData(viewModel: Home.Load.ViewModel)
    func displayMoviesCategoryPage(viewModel: Home.Movies.ViewModel)
}

class HomeViewController: UIViewController {
    
    // MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    private var tableViewHandler: TableViewDataSource<HomeTableViewCell, CategoryModel>?
    
    // MARK:- Constants
    private struct Constants {
        static let title = "Movies Challenge"
        static let cellIdentifier = "Cell"
    }
  
    // MARK:- Variables
    var categories = AllCategoriesModel()
    var interactor: HomeBusinessLogic?
    var router: (NSObjectProtocol & HomeRoutingLogic & HomeDataPassing)?

    // MARK: Object lifecycle
  
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setupCleanArchStack()
    }
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupCleanArchStack()
    }
  
    // MARK: Setup
  
    private func setupCleanArchStack() {
        let viewController = self
        let interactor = HomeInteractor()
        let presenter = HomePresenter()
        let router = HomeRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
        self.title = Constants.title
    }
    
    private func setupUI() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .never
    }
    
    private func setupTableView() {
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        self.tableViewHandler = TableViewDataSource<HomeTableViewCell, CategoryModel>(Constants.cellIdentifier, categories, cellConfigurationBlock: { (cell, category) in
            
            cell.titleLabel.text = category.first?.key
            
        }, cellSelectorHandler: { (category) in
            
            let category = Home.Movies.Request(category: category)
            self.interactor?.goToMovieCategoryList(request: category)
        })
        self.tableView.dataSource = tableViewHandler
        self.tableView.delegate = tableViewHandler
    }
  
    // MARK: View lifecycle
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        loadInitialData()
    }
  
    func loadInitialData() {
        let request = Home.Load.Request()
        interactor?.doLoadInitialData(request: request)
    }
}

extension HomeViewController: HomeDisplayLogic {
    
    func displayInitialData(viewModel: Home.Load.ViewModel) {
        self.categories = viewModel.categories
        DispatchQueue.main.async {
            self.setupTableView()
            self.tableView.reloadData()
        }
    }
    
    func displayMoviesCategoryPage(viewModel: Home.Movies.ViewModel) {
        router?.routeToMoviesList()
    }
}

