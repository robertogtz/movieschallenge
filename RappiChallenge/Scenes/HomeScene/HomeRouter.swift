//
//  HomeRouter.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

@objc protocol HomeRoutingLogic {
    func routeToMoviesList()
}

protocol HomeDataPassing {
    var dataStore: HomeDataStore? { get }
}

class HomeRouter: NSObject, HomeRoutingLogic, HomeDataPassing {
    
    weak var viewController: HomeViewController?
    var dataStore: HomeDataStore?
    
    func routeToMoviesList() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: "MovieListViewController") as! MovieListViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToComments(source: dataStore!, destination: &destinationDS)
        navigateToComments(source: viewController!, destination: destinationVC)
    }
    
    // MARK: Navigation
    func navigateToComments(source: HomeViewController, destination: MovieListViewController) {
        source.show(destination, sender: nil)
    }
    
    // MARK: Passing data
    
    func passDataToComments(source: HomeDataStore, destination: inout MovieListDataStore) {
        destination.category = source.category
    }
  
}
