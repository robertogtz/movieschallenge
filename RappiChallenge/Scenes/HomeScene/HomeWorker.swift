//
//  HomeWorker.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

class HomeWorker {
    
    private struct Constants {
        static let categoriesFileName = "categories.json"
    }
  
    func loadInitialData(completion: @escaping() -> Void) {
        NetworkManager.shared.getAllCategories { (categoriesResponse) in
            if categoriesResponse.count > 0 {
                Storage.store(categoriesResponse, to: .caches, as: Constants.categoriesFileName)
                completion()
            }
        }
    }
    
    func retrieveData() -> AllCategoriesModel {
        return Storage.retrieve(Constants.categoriesFileName, from: .caches, as: AllCategoriesModel.self)
    }
}
