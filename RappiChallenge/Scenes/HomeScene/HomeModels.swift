//
//  HomeModels.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

enum Home {
    
    // Note: some of this are empty, but still im passing them as parameter, to make app more scalable. Maybe later can be filled
    enum Load {
        struct Request {}
        struct Response {
            let categories: AllCategoriesModel
        }
        struct ViewModel {
            let categories: AllCategoriesModel
        }
    }
    
    enum Movies {
        struct Request {
            let category: CategoryModel
        }
        struct Response {
            let category: CategoryModel
        }
        struct ViewModel {
            let category: CategoryModel
        }
    }
}
