//
//  HomeInteractor.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol HomeBusinessLogic {
    func doLoadInitialData(request: Home.Load.Request)
    func goToMovieCategoryList(request: Home.Movies.Request)
}

protocol HomeDataStore {
    var category: CategoryModel { get set }
}

class HomeInteractor: HomeBusinessLogic, HomeDataStore {
   
    var category = CategoryModel()
    
    var presenter: HomePresentationLogic?
    var worker: HomeWorker?
    
    func doLoadInitialData(request: Home.Load.Request) {
        
        worker = HomeWorker()
        worker?.loadInitialData(completion: {
            guard let allCategories = self.worker?.retrieveData() else { return }
            let response = Home.Load.Response(categories: allCategories)
            self.presenter?.presentInitialData(response: response)
        })
    }
    
    func goToMovieCategoryList(request: Home.Movies.Request) {
        self.category = request.category
        let response = Home.Movies.Response(category: self.category)
        presenter?.presentMovieCategory(response: response)
    }
}
