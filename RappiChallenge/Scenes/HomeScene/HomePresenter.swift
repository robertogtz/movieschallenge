//
//  HomePresenter.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright (c) 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import UIKit

protocol HomePresentationLogic {
    func presentInitialData(response: Home.Load.Response)
    func presentMovieCategory(response: Home.Movies.Response)
}

class HomePresenter: HomePresentationLogic {
    
    weak var viewController: HomeDisplayLogic?
  
    func presentInitialData(response: Home.Load.Response) {
        let viewModel = Home.Load.ViewModel(categories: response.categories)
        viewController?.displayInitialData(viewModel: viewModel)
    }
    
    func presentMovieCategory(response: Home.Movies.Response) {
        let viewModel = Home.Movies.ViewModel(category: response.category)
        viewController?.displayMoviesCategoryPage(viewModel: viewModel)
    }
}
