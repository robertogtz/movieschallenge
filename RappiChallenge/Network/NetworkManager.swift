//
//  NetworkManager.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import Foundation

typealias CategoryModel = [String: [Movie]]
typealias AllCategoriesModel = [CategoryModel]

class NetworkManager {
    
    //MARK: - Constants
    private struct Constants {
        static let popularMovies  = "Popular"
        static let upcomingMovies = "Upcoming"
        static let topRatedMovies = "Top Rated"
    }
    
    //MARK: - Singleton
    static let shared = NetworkManager()
    
    //MARK: - Private init
    private init() {}
    
    func getAllCategories(completion: @escaping (AllCategoriesModel) -> Void) {
        var response = AllCategoriesModel()
        let queue = OperationQueue()
        
        let getInfoOperation = BlockOperation {
            let group = DispatchGroup()
            
            group.enter()
            self.getCategoryResponse(for: .popular, completion: { (popularResponse) in
                response.append(popularResponse)
                group.leave()
            })
            
            group.enter()
            self.getCategoryResponse(for: .topRated, completion: { (topRatedResponse) in
                response.append(topRatedResponse)
                group.leave()
            })
            
            group.enter()
            self.getCategoryResponse(for: .upcoming, completion: { (upcomingResponse) in
                response.append(upcomingResponse)
                group.leave()
            })
        
            group.wait()
        }
        let passInfoOperation = BlockOperation {
            completion(response)
        }
        passInfoOperation.addDependency(getInfoOperation)
        queue.addOperation(getInfoOperation)
        queue.addOperation(passInfoOperation)
    }
    
    
    private func getMovies(for movieCategory: Category, completion: @escaping (MoviesResponse?) -> Void ) {
    
        let dataTask = URLSession.shared.dataTask(with: URLBuilder.url(for: movieCategory)) { (data, response, error) in
            DispatchQueue.main.async {
                if let data = data {
                    let decoder = CodableParser.createDecoder()
                    do {
                        let decodedPosts = try decoder.decode(MoviesResponse.self, from: data)
                        completion(decodedPosts)
                    } catch let err {
                        print(err.localizedDescription)
                        completion(nil)
                    }
                }
            }
        }
        dataTask.resume()
    }
    
    private func getCategoryResponse(for movieCategory: Category, completion: @escaping (CategoryModel) -> Void ) {
        var key: String
        switch movieCategory {
        case .popular:
            key = Constants.popularMovies
            break
        case.topRated:
            key = Constants.topRatedMovies
            break
        case.upcoming:
            key = Constants.upcomingMovies
            break
        }
        getMovies(for: movieCategory) { (response) in
            var category = CategoryModel()
            category[key] = response?.results
            completion(category)
        }
    }
}

class CodableParser {
    
    class func createDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .useDefaultKeys
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }
    
}
