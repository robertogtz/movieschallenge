//
//  URLBuilder.swift
//  RappiChallenge
//
//  Created by Roberto Gutierrez Gonzalez on 7/6/19.
//  Copyright © 2019 Roberto Gutierrez Gonzalez. All rights reserved.
//

import Foundation

class URLBuilder {
    
    //MARK: - Constants
    private struct Constants {
        static let baseURL        = "https://api.themoviedb.org/3/movie/"
        static let apiKey         = "?api_key=d7f77038021a3685a9b4fcea36ecdb4b"
        static let imageBaseURL   = "https://image.tmdb.org/t/p/w200"
        static let popularMovies  = "popular"
        static let upcomingMovies = "upcoming"
        static let topRatedMovies = "top_rated"
    }
    
    static func url(for urlType: Category) -> URL {
        var urlString = Constants.baseURL
        switch urlType {
        case .popular:
            urlString += Constants.popularMovies
            break
        case .topRated:
            urlString += Constants.topRatedMovies
            break
        case .upcoming:
            urlString += Constants.upcomingMovies
            break
        }
        urlString += Constants.apiKey
        return URL(string: urlString)!
    }
}

enum Category {
    case popular
    case topRated
    case upcoming
}
